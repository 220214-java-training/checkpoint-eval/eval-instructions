# Checkpoint Evaluation
- This evaluation is meant to show your ability to demonstrate the use the technologies covered in training so far.
- You will have until 5 pm CST today to complete the 5 phases listed below.
- You may use google, any notes, previous examples, or project work to complete this task. However, you are **not** able to collaborate. Your work should be a reflection of your own ability.

---

The goal of this application is to retrieve a JSON representation of e-commerce items. These items will be stored in your database, and making a GET request to /items should return a JSON representation of all of the db records. The project should be completed in phases, and your progress should be pushed to gitlab throughout the day.

## Phase 1 - Define Your Schema
- Create a table in your database
- This table should represent an item being sold on an e-commerce application
- The record should include at least an id, a name, a price, and a category

## Phase 2 - Create Your Maven Project
- Create a Maven project in IntelliJ
- Include the dependencies for postgres, jackson-databind, and the servlet api
- Save the SQL script you used to create your tables in this project folder as well

** at this point you should create a local repository and push it to a remote gitlab repository in your "checkpoint-eval" group - commit message should read "Phase 2 Complete" ** 

## Phase 3 - Define Your Java Model
- Define a Java class to represent your e-commerce item. Your class should define the appropriate instance variables, be properly encapsulated, and override any appropriate Object class methods.

** push your progress to your remote repo - commit message should read "Phase 3 Complete" **

## Phase 4 - Define Your DAO
- Create an ItemDAO interface. This interface should contain any necessary data access method(s) you may need to retrieve the data in your db.
- Implement the interface using JDBC. Test this method using a unit test (junit dependency needed) or in a main method to verify it's working as expected.

** push your progress to your remote repo - commit message should read "Phase 4 Complete" **

## Phase 5 - Configure Your Servlet
- Making a GET request to /items should return a JSON list of items.
- Create a servlet which handles that request and responds accordingly.

** push your progress to your remote repo - commit message should read "Phase 5 Complete" **

### Other Requirements:
- code should be properly organized, using the appropriate package structure and Java naming conventions
- each phase submission should be free of compilation errors